import os
import numpy as np

measurement_name = input("Measurement name: ")

folder = "%s/%s" % (os.getcwd(), measurement_name)

files = os.listdir(folder)

measurements = dict()
measurements_index = dict()

measurements_merge = dict()
measurements_merge_test = dict()

for f in files:
    x, y, t = f.split('_')

    x = int(x)
    y = int(y)

    if measurements.get(x) is None:
        measurements[x] = dict()
        measurements_index[x] = dict()

    if measurements[x].get(y) is None:
        measurements[x][y] = dict()
        measurements_index[x][y] = []
        measurements_merge[(x, y)] = None
        measurements_merge_test[(x, y)] = None

    measurements[x][y][t] = np.load("%s/%s" % (folder, f))
    measurements_index[x][y].append(t)

for x in measurements_index.keys():
    for y in measurements_index[x].keys():
        i = measurements_index[x][y]

        measurements_merge[(x, y)] = measurements[x][y][
            i[-5]
        ]

        measurements_merge_test[(x, y)] = measurements[x][y][
            i[-1]
        ]

np.save(
    "%s/data/%s.npy" % (os.getcwd(), measurement_name),
    measurements_merge
)

np.save(
    "%s/data/%s_test.npy" % (os.getcwd(), measurement_name),
    measurements_merge_test
)

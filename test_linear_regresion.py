import load_data

from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.multioutput import MultiOutputRegressor

X, y, X_t, y_t = load_data.load('measurements_big_metal_ball')

m = MultiOutputRegressor(
    svm.SVR(),
    # n_jobs=-1,
)
gamma = []
gamma.append('auto')

#for g in 1 / np.arange(1, 10, 5):
#    gamma.append(g)

grid_search = GridSearchCV(
    m,
    param_grid={
        'estimator__kernel': ['linear', 'poly', 'rbf'],
        'estimator__degree': range(2, 6, 1),
        'estimator__gamma': gamma,
    },
    cv=3,
)

print(grid_search.get_params())

grid_search.fit(X, y)

print(grid_search.best_params_)

y_hat = grid_search.predict(X)
Y = y - y_hat

print(Y)

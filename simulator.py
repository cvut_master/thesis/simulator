from typing import Tuple

import numpy as np
import pandas as pd
from dataclasses import asdict
import matplotlib
import matplotlib.pyplot as plt
import functools
from pvtrace.light.utils import wavelength_to_rgb
from pvtrace.scene.scene import Scene
from pvtrace.trace.tracer import PhotonTracer
from pvtrace.common.errors import TraceError
from pvtrace.light.light import Light
from pvtrace.light.ray import Ray
from pvtrace.geometry.mesh import Mesh
from pvtrace.geometry.box import Box
from pvtrace.geometry.sphere import Sphere
from pvtrace.material.material import Dielectric, LossyDielectric, Lumophore, Host
from pvtrace.scene.node import Node
from pvtrace.material.distribution import Distribution
import logging

# We want to see pvtrace logging here
logging.getLogger('pvtrace').setLevel(logging.WARNING)

scale = 0.1
light_bed_height = 1.55 * scale

world_range = (300, 1000.0)

wavelength_range = (890, 990)
wavelength = np.linspace(*wavelength_range)

## LSC scene
# Make a world coordinate system
world_node = Node(name='world')
world_node.geometry = Sphere(radius=1000.0 * scale, material=Dielectric.air(world_range))

# Add LSC
size: Tuple[float, float, float] = (50.0 * scale, 50.0 * scale, light_bed_height)
lsc = Node(name="LightBed", parent=world_node)
lsc.geometry = Box(
    size,
    material=LossyDielectric.make_constant(x_range=world_range, refractive_index=1.4823, absorption_coefficient=1000.0)
)
lsc.translate((0, 0, light_bed_height / 2))

sphere = Node(name="Sphere (Fe)", parent=world_node)
sphere.geometry = Sphere(
    radius=3.0 * scale,
    # material=LossyDielectric.make_constant(x_range=world_range, refractive_index=2.9391, absorption_coefficient=1.0),
    material=LossyDielectric.make_constant(
        x_range=world_range,
        refractive_index=1000,
        # kinda a hack, used library do not support conductive materials yet
        # high refractive index makes object reflective @see material.interface.generate_interaction_sequence
        absorption_coefficient=10.0,
        # next hack
        # high absorption index kill ray after realy short distance in the material
        # - rays pass previous hack with refractance are killed here
    ),
    # material=Dielectric.glass(),
)
sphere.translate((
    5.0 * scale,
    5.0 * scale,
    light_bed_height + sphere.geometry.radius + .01 * scale,
))

# Light source
x = wavelength
y = (1. - np.abs(x - 940) / 50)
dist = Distribution(x, y)

light_bound_min = -75
light_bound_max = 75
light_phi = light_bound_min
light_theta = light_bound_min

light_step = 1

rays_count: int = ((light_bound_max - light_bound_min) / light_step + 1) \
                  * ((light_bound_max - light_bound_min) / light_step + 1)


def light_divergence():
    global light_phi, light_theta, light_step

    phi = light_phi
    theta = light_theta

    light_phi += light_step

    if light_phi > light_bound_max:
        light_phi = light_bound_min

        light_theta += light_step

        if light_theta > light_bound_max:
            light_theta = light_bound_min

    return np.radians([phi, theta])


if False:
    rays_count = 1


    def light_divergence():
        return [0, 0]

light = Light(
    wavelength_delegate=lambda: dist.sample(np.random.uniform()),
    # divergence_delegate=functools.partial(Light.cone_divergence, np.radians(75)),
    divergence_delegate=light_divergence,
)
light_node = Node(
    name='light', parent=world_node,
    location=(
        4. * scale,
        4. * scale,
        light_bed_height + 0.001 * scale,  # offset is required, otherwise framework crashes
    ),
)
light_node.light = light

if False:
    fig, ax1 = plt.subplots()
    ax1.plot(x, y, label="Relative intensity VS Wavelength")
    ax1.set_ylabel('Relative intensity')
    ax1.set_xlabel('Wavelength (nm)')
    plt.grid(linestyle='dotted')
    ax1.legend()

scene = Scene(root=world_node)

# This enable 3D renderer in browser - nice to view what is happening
if False:
    from pvtrace.scene.renderer import MeshcatRenderer

    renderer = MeshcatRenderer(max_histories=None)
    renderer.render(scene)

    tracer = PhotonTracer(scene)
    for _light_node in scene.light_nodes:
        for ray in light.emit(rays_count):
            ray = ray.representation(_light_node, world_node)
            history = tracer.follow(ray)
            path = [x for x in history]
            renderer.add_ray_path(path)

    renderer.vis.jupyter_cell()

from ipywidgets import IntProgress
from IPython.display import display

np.random.seed(4)
tracer = PhotonTracer(scene)
entrance_rays = []
exit_rays = []
max_rays = rays_count
f = IntProgress(min=0, max=max_rays, description='Tracing:')

display(f)

ten_percent_milestone = max_rays / 100

for _light_node in scene.light_nodes:
    for idx, ray in enumerate(light.emit(max_rays)):
        ray = ray.representation(_light_node, world_node)
        try:
            path = tracer.follow(ray)
        except TraceError:
            continue
        else:
            if len(path) > 2 and path[2].direction[2] < 0:
                entrance_rays.append(path[0])
                exit_rays.append(path[-2])  # -1 is the world node, don't want that
        finally:
            f.value += 1

            if 0 < (f.value % ten_percent_milestone) < 1:
                print("%2.2f%%" % (100 * f.value / max_rays))
print("Done!")


def expand_coords(df, column):
    """ Returns a dataframe with coordinate column expanded into components.

        Parameters
        ----------
        df : pandas.DataFrame
            The dataframe
        column : str
            The column label

        Returns
        -------
        df : pandas.DataFrame
            The dataframe with the column expanded.

        Example
        -------
        Given the dataframe::

            df = pd.DataFrame({'position': [(1,2,3)]})

        the function will return a new dataframe::

            edf = expand_coords(df, 'position')
            edf == pd.DataFrame({'position_x': [1], 'position_y': [2], 'position_z': [3]})

    """
    coords = np.stack(df[column].values)
    df['{}_x'.format(column)] = coords[:, 0]
    df['{}_y'.format(column)] = coords[:, 1]
    df['{}_z'.format(column)] = coords[:, 2]
    df.drop(columns=column, inplace=True)
    return df


def label_facets(df, length, width, height):
    """ Label rows with facet names for a box LSC.

        Notes
        -----
        This function only works if the coordinates in the dataframe
        are in the local frame of the box. If the coordinates are in the
        world frame then this will still work provided the box is axis
        aligned with the world node and centred at the origin.
    """
    xmin, xmax = -0.5 * length, 0.5 * length
    ymin, ymax = -0.5 * width, 0.5 * width
    zmin, zmax = -0.5 * height, 0.5 * height
    df.loc[(np.isclose(df['position_x'], xmin)), 'facet'] = '-x'
    df.loc[(np.isclose(df['position_x'], xmax)), 'facet'] = '+x'
    df.loc[(np.isclose(df['position_y'], ymin)), 'facet'] = '-y'
    df.loc[(np.isclose(df['position_y'], ymax)), 'facet'] = '+y'
    df.loc[(np.isclose(df['position_z'], zmin)), 'facet'] = '-z'
    df.loc[(np.isclose(df['position_z'], zmax)), 'facet'] = '+z'
    return df


df = pd.DataFrame()

# Rays entering the scene
for ray in entrance_rays:
    rep = asdict(ray)
    rep['kind'] = 'entrance'
    df = df.append(rep, ignore_index=True)

# Rays exiting the scene
for ray in exit_rays:
    rep = asdict(ray)
    rep['kind'] = 'exit'
    df = df.append(rep, ignore_index=True)

# Expand position and direction columns into component columns
df = expand_coords(df, 'direction')
df = expand_coords(df, 'position')

# Label facets
df = label_facets(df, *size)
df.head()


# print(df)


def w_norm(w):
    """ Lowers IR wavelength to visible range (color wave constant are reverse engineered from the 'wavelength_to_rgb' method)
    """
    colorwave_range = (580, 645)

    norm = (w - wavelength_range[0]) / (wavelength_range[1] - wavelength_range[0])

    return norm * (colorwave_range[1] - colorwave_range[0]) + colorwave_range[0]


def xy_plot(df):
    """ Plots ray positions in the xy-plane.

        References
        ----------
        [1] https://stackoverflow.com/questions/44959955/matplotlib-color-under-curve-based-on-spectral-color
    """
    norm = plt.Normalize(*wavelength_range)
    wl = np.arange(wavelength_range[0], wavelength_range[1] + 1, 2)
    colorlist = list(zip(norm(wl), [np.array(wavelength_to_rgb(w_norm(w))) / 255 for w in wl]))
    spectralmap = matplotlib.colors.LinearSegmentedColormap.from_list("spectrum", colorlist)
    colors = [spectralmap(norm(value)) for value in df['wavelength']]
    df.plot(x='position_x', y='position_y', kind='scatter', alpha=1.0, color=colors)
    plt.axis('equal')

    plt.show()


xy_plot(df)

counts = dict()
counts['-x'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '-x')].shape[0]
counts['+x'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '+x')].shape[0]
counts['-y'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '-y')].shape[0]
counts['+y'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '+y')].shape[0]
counts['-z'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '-z')].shape[0]
counts['+z'] = df.loc[(df['kind'] == 'exit') & (df['facet'] == '+z')].shape[0]
print(counts)

etaopt = dict()
thrown = df[df['kind'] == 'entrance'].size
for facet in counts:
    etaopt[facet] = counts[facet] / thrown

print(etaopt)

print(etaopt['-x'] + etaopt['+x'] + etaopt['-y'] + etaopt['+y'])

input("Hit any key to continue...")

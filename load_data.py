import os
from typing import Dict, Tuple

import numpy as np

FOLDER = 'data'


def load(measurement: str):
    data_file = "%s/%s/%s.npy" % (os.getcwd(), FOLDER, measurement)
    data_test_file = "%s/%s/%s_test.npy" % (os.getcwd(), FOLDER, measurement)

    data: Dict[Tuple[int, int], np.ndarray] = np.load(data_file, allow_pickle=True).item()
    data_test: Dict[Tuple[int, int], np.ndarray] = np.load(data_test_file, allow_pickle=True).item()

    y = []
    X = []

    for i in data.keys():
        d = data[i][1:]  # drop first measurement - data about surrounding light

        y.append([i[0], i[1]])
        X.append(d.reshape((d.shape[0] * d.shape[1],)))

    y_t = []
    X_t = []

    for i in data_test.keys():
        d = data_test[i][1:]  # drop first measurement - data about surrounding light

        y_t.append([i[0], i[1]])
        X_t.append(d.reshape((d.shape[0] * d.shape[1],)))

    return X, y, X_t, y_t

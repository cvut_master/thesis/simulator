import load_data
import numpy as np

from matplotlib import pyplot as plt
import seaborn as sb

X, y, X_t, y_t = load_data.load('measurements_big_metal_ball')

fig = plt.figure(figsize=(15, 15))
sb.heatmap(X[0])

plt.show()

input('')

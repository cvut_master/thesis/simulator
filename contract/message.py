from __future__ import annotations
import numpy as np


class Message(object):
    measurement: np.ndarray
    device_id: int
    bank_id: int

    def __init__(self, device_id: int, bank_id: int, measurement: np.ndarray):
        self.device_id = device_id
        self.bank_id = bank_id
        self.measurement = measurement

    @classmethod
    def make_from_packet(cls, packet: bytes, measurement_count: int):
        if len(packet) != (measurement_count * 2 + 2):
            raise ValueError(
                "Packet is %d bytes length, %d bytes expected" % (len(packet), (measurement_count * 2 + 2))
            )

        device_id = int.from_bytes(packet[0:1], 'big')
        bank_id = int.from_bytes(packet[1:2], 'big')

        measurement = np.ndarray((measurement_count,), np.int64)
        for i in range(0, measurement_count):
            m = packet[2 + i * 2:2 + i * 2 + 2]

            measurement[i] = int.from_bytes(m, 'big')

        return cls(device_id, bank_id, measurement)

import socket
import numpy as np
import time
import os
import threading
from typing import List

from contract.message import Message

LOCAL_IP = '0.0.0.0'
UDP_IP = '10.15.150.138'
ANNOUNCE_PORT = 10063
UDP_PORT = 10064

MEASUREMENT_FOLDER = 'measurements'

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((LOCAL_IP, UDP_PORT))
sock.settimeout(10.)  # 10s timeout


def announce():
    announce_message: bytes = str.encode("0.0.0.0:%d\n" % UDP_PORT)

    announce_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        announce_sock.sendto(announce_message, (UDP_IP, ANNOUNCE_PORT))
    except OSError:
        pass


def store_measurements(measurements: List[Message], measurement_prefix: str, store_name_prefix: str):
    serialize = np.ndarray((len(measurements), len(measurements[0].measurement)), np.int64)

    for i in measurements:
        serialize[i.bank_id] = i.measurement

    np.save("%s/%s_%s/%s_%.5f.npy" % (
        os.getcwd(),
        MEASUREMENT_FOLDER,
        measurement_prefix,
        store_name_prefix,
        time.time(),
    ), serialize)


announce()


class ThreadContext(object):
    k: int
    store_name_prefix: str
    measurement_prefix: str
    measurements: List[Message]
    measurement_count: int

    def __init__(self):
        self.k = 0
        self.store_name_prefix = ''
        self.measurement_prefix = ''
        self.measurement_count = 64
        self.measurements: List[Message] = [None for x in range(
            self.measurement_count + 1  # offset for measurement without influence of controlled IR LED
        )]


thread_context = ThreadContext()


def thread_receiver(context: ThreadContext):
    message_buffer = context.measurement_count * 2 + 2

    start_time = time.time()
    while True:
        try:
            data, addr = sock.recvfrom(
                message_buffer + 8  # offset to make possible detection of bigger then supported message
            )

            if len(data) != message_buffer:
                raise ValueError(
                    "Packet is %d bytes length, %d bytes expected" % (len(data), message_buffer)
                )

            msg = Message.make_from_packet(data, context.measurement_count)

            context.measurements[msg.bank_id] = msg

            if msg.bank_id == 0:
                start_time = time.time()
            if msg.bank_id == 64 and context.measurements[0] is not None:
                t = time.time() - start_time
                # print("%.6fs - %.3f message/sec" % (t, 1 / t))

                if context.k < 10:
                    store_measurements(
                        context.measurements,
                        thread_context.measurement_prefix,
                        context.store_name_prefix,
                    )

                    context.k += 1
        except socket.timeout:
            announce()
        except ValueError as err:
            print(err)


x = 0
y = 0


def update_prefix():
    global x, y, thread_context

    thread_context.store_name_prefix = "%02d_%02d" % (y, x)


def increase_prefix():
    global x, y

    x += 5

    if x >= 45:
        x = 0
        y += 5

        if y > 45:
            raise InterruptedError


if __name__ == "__main__":
    thread_context.measurement_prefix = input("Measurement name: ")
    os.mkdir("%s/%s_%s" % (
        os.getcwd(),
        MEASUREMENT_FOLDER,
        thread_context.measurement_prefix,
    ))

    update_prefix()

    receiver = threading.Thread(target=thread_receiver, args=(thread_context,))
    receiver.start()

    while True:
        input("Hit enter")
        increase_prefix()
        update_prefix()

        print(thread_context.store_name_prefix)

        thread_context.k = 0
